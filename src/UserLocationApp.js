import React, { useEffect, useState, useRef } from 'react'
import 'antd/dist/antd.css'
import { Button, Space, Input, Row, Col, Form, Checkbox, Table } from 'antd'
import UseGeoLocation from './UseGeoLocation'
import DrawMap from './DrawMap';
import { v4 as uuidv4 } from 'uuid'

function UserLocationApp() {
    const [location_form] = Form.useForm()
    const history_tb = useRef()
    const location = UseGeoLocation()
    const api_key = 'AIzaSyDcuHGyq-_aGIC9BbS95zIXD6sauUnRqzs'
    const map_zoom = 11
    const [pins, setPins] = useState([])
    const [histories, setHistories] = useState([])
    const [selectedHistories, setSelectedHistories] = useState([])
    var lat = location.coordinates.lat
    var lng = location.coordinates.lng

    const history_columns = [
        {
            title: 'ID',
            dataIndex: 'order',
            key: 'history_tb_id',
        },
        {
            title: 'Latitude',
            dataIndex: 'lat',
            key: 'history_tb_lat',
        },
        {
            title: 'Longitude ',
            dataIndex: 'lng',
            key: 'history_tb_lng',
        },
        {
            title: 'Location',
            dataIndex: 'location',
            key: 'history_tb_loc',
        },
        {
            title: 'Timezone',
            dataIndex: 'timezone',
            key: 'history_tb_timezone'
        },
        {
            title: 'Local Time',
            dataIndex: 'localtime',
            key: 'history_tb_localtime'
        },
    ];

    useEffect(() => {
        if (location.loaded)  {
            lat = location.coordinates.lat
            lng = location.coordinates.lng
        }
    }, [location.loaded])

    async function fetchLocation() {
        var json = {}

        try {
            const response = await fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + lat + ',' + lng + '&key=' + api_key)
            json = await response.json()
        }
        catch (err) {
            throw err
        }

        if (json.results) {
            return json.results[0].formatted_address
        }
    }

    function getLocation() {
        fetchLocation()
        .then((result) => { 
            location_form.setFieldsValue({
                location: result
            })
        })
    }

    function addPin(lat, lng, text) {
        const pin_id = uuidv4()
        setPins(prevPins => {
            return [...prevPins, { id: pin_id, lat: lat, lng: lng, text: text }]
        })
        return pin_id
    }

    function addHistory(pin_id, lat, lng, text, timezone, localtime) {
        setHistories(prevHistories => {
            return [...prevHistories, { key: uuidv4(), pin_id: pin_id, order: prevHistories.length + 1, lat: lat, lng: lng, location: text, timezone: timezone, localtime: localtime }]
        })
    }

    async function fetchLocationWithText(text) {
        var json = {}

        try {
            const response = await fetch('https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/place/textsearch/json?query=' + text + '&key=' + api_key)
            json = await response.json()
        }
        catch (err) {
            throw err
        }

        if (json.results) {
            return json.results[0].geometry.location
        }
    }

    function searchLocation(text) {
        fetchLocationWithText(text)
        .then((loc) => {
            var pin = addPin(loc.lat, loc.lng, text)
            fetchTimezone(loc.lat, loc.lng)
            .then((ts) => {
                addHistory(pin, loc.lat, loc.lng, text, ts.timezone, ts.localtime)
            })
        })
    }


    async function fetchTimezone(lat, lng) {
        var json = {}
        var daysofweek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun']
        var targetDate = new Date()
        var timestamp = targetDate.getTime( )/ 1000 + targetDate.getTimezoneOffset() * 60

        try {
            const response = await fetch('https://maps.googleapis.com/maps/api/timezone/json?location=' + lat + '%2C' + lng + '&timestamp=' + timestamp + '&key=' + api_key)
            json = await response.json()
        }
        catch (err) {
            throw err
        }

        if (json.status == 'OK') {
            var offsets = json.dstOffset * 1000 + json.rawOffset * 1000
            var localdate = new Date(timestamp * 1000 + offsets)
            var refreshDate = new Date()
            var millisecondselapsed = refreshDate - targetDate
            localdate.setMilliseconds(localdate.getMilliseconds() + millisecondselapsed)

            return { localtime: localdate.toLocaleTimeString() + ' (' + daysofweek[ localdate.getDay() ] + ')', timezone: json.timeZoneName }
        }
    }

    function removeSearchRecord() {
        selectedHistories.sort((a, b) => b.order - a.order)
        console.log(selectedHistories)
        for (var i = 0; i < selectedHistories.length; i++) {
            const selectedHistory = selectedHistories[i]
            const index = histories.findIndex((history) => {
                return history.key == selectedHistory.key
            })
            setHistories(prevHistories => { 
                var removedHistory = prevHistories.splice(index, 1)
                document.getElementById(removedHistory[0].pin_id).remove()
                return [...prevHistories] 
            })
        }
        setSelectedHistories([])
    }

    const [bottom, setBottom] = useState('bottomCenter');
    const [selectionType, setSelectionType] = useState('checkbox');
    const rowSelection = {
        onChange: (key, selected) => {
            setSelectedHistories(prevSelectedHistories => { return [...selected] })
        },
        getCheckboxProps: (record) => ({}),
    };
    return (
        <>
            <Row justify='center' align='middle' style={{ height: '20vh' }}>
                <Col>
                    <Space direction='vertical'>
                        <Form.Provider
                            onFormFinish={name => {
                                if (name === 'location_form') {
                                    searchLocation(location_form.getFieldValue('location'))
                                }
                            }}
                        >
                            <Form form={location_form} name='location_form' autoComplete='off'>
                                <Space>
                                    <Form.Item label='Location' name='location' rules={[{ required: true, message: 'Please input your location!' }]}>
                                        <Input />
                                    </Form.Item>
                                    <Form.Item name='curr_location'>
                                        <Button onClick={getLocation}>Current Location</Button>
                                    </Form.Item>
                                </Space>
                                <Form.Item style={{ textAlign: 'center' }}>
                                    <Button type='primary' htmlType='submit'>
                                        Search
                                    </Button>
                                </Form.Item>
                            </Form>
                        </Form.Provider>
                    </Space>
                </Col>
            </Row>
            <Row justify='center' style={{ paddingBottom: '5px' }}>
                <Col span={22}>
                    User Action: <br/>
                    <Button type='primary' onClick={removeSearchRecord} danger>Remove</Button>
                </Col>
            </Row>
            <Row justify='center'>
                <Col span={11}>
                    <Row align='top' style={{ maxHeight: '100%' }}>
                        <Col span={24} style={{ height: '100%' }}>
                            <Table 
                                ref={history_tb}
                                columns={history_columns} 
                                dataSource={[...histories]} 
                                pagination={{ 
                                    position: [bottom],
                                    pageSize: 10,
                                }}
                                size='small'
                                rowSelection={{
                                    type: selectionType,
                                    ...rowSelection,
                                }}
                                style={{ height: '100%', justifyContent: 'center' }} 
                            />
                        </Col>
                    </Row>
                </Col>
                <Col span={11} style={{ height: '50vh', width: '100%' }}>
                    <DrawMap lat={lat} lng={lng} pins={pins} zoom={map_zoom} api_key={api_key} />
                </Col>
            </Row>
        </>
    );
}

export default UserLocationApp;