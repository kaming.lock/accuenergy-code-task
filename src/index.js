import React from 'react';
import ReactDOM from 'react-dom/client';
import UserLocationApp from './UserLocationApp';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // <React.StrictMode>
    <UserLocationApp />
  // </React.StrictMode>
);