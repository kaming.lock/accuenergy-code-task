import React from 'react'
import './MapPin.css';

const AnyReactComponent = ({ id, lat, lng, text }) => <div id={id} className='pin'>{text}</div>;

export default function MapPin ({ id, lat, lng, text }) {
    return (
        <AnyReactComponent id={id} lat={lat} lng={lng} text={text} />
    )
}
