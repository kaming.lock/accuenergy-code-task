import React from "react";
import GoogleMapReact from 'google-map-react';
import MapPin from './MapPin'

export default function DrawMap({ lat, lng, pins, zoom, api_key }) {
    const defaultProps = {
        center: {
            lat: lat ? parseFloat(lat) : 0,
            lng: lng ? parseFloat(lng) : 0,
        },
        zoom: zoom ?? 0
    };

    return (
        <GoogleMapReact
            bootstrapURLKeys={{ key: api_key }}
            defaultCenter={defaultProps.center}
            defaultZoom={defaultProps.zoom}
        >
            {pins.map(pin => {
                return <MapPin key={pin.id} id={pin.id} lat={pin.lat} lng={pin.lng} text={pin.text} />
            })}
        </GoogleMapReact>
    );
}